<?php

namespace App\Controller;

use App\Entity\Restaurant;
use App\Entity\RestaurantTable;
use App\Form\RestaurantTableFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/restaurant-table", name="app_restaurant_table_")
 * @package App\Controller
 */
class RestaurantTableController extends AbstractController
{
    /**
     * @Route("/create/{restaurantId}", name="create")
     * @param Request $request
     * @param $restaurantId
     * @return RedirectResponse|Response
     */
    public function createRestaurantTable(Request $request, $restaurantId)
    {
        $restaurantTable = new RestaurantTable();
        $restaurantRepository = $this->getDoctrine()->getRepository(Restaurant::class);
        /** @var Restaurant $restaurant */
        $restaurant = $restaurantRepository->find($restaurantId);

        $form = $this->createForm(RestaurantTableFormType::class, $restaurantTable);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $restaurantTable->setRestaurant($restaurant);
            $restaurantTable->setStatus(RestaurantTable::RESTAURANT_TABLE_STATUS_INACTIVE);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($restaurantTable);
            $entityManager->flush();

            $this->addFlash(
                'success',
                'Restaurant table with id: ' . $restaurantTable->getId() . ' has been created successfully!'
            );

            return $this->redirectToRoute('app_restaurant_view', [
                'restaurantId' => $restaurantId
            ]);
        }

        return $this->render('restaurant_table/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{restaurantTableId}", name="edit")
     * @param Request $request
     * @param $restaurantTableId
     * @return RedirectResponse|Response
     */
    public function editRestaurantTable(Request $request, $restaurantTableId)
    {
        $restaurantTableRepository = $this->getDoctrine()->getRepository(RestaurantTable::class);
        $restaurantTable = $restaurantTableRepository->find($restaurantTableId);

        $form = $this->createForm(RestaurantTableFormType::class, $restaurantTable);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($restaurantTable);
            $entityManager->flush();

            $this->addFlash(
                'success',
                'Restaurant table with id: ' . $restaurantTable->getId() . ' has been edited successfully!'
            );

            return $this->redirectToRoute('app_restaurant_view', [
                'restaurantId' => $restaurantTable->getRestaurant()->getId()
            ]);
        }

        return $this->render('restaurant_table/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/remove/{restaurantTableId}", name="remove")
     * @param $restaurantTableId
     * @return RedirectResponse
     */
    public function deleteRestaurantTable($restaurantTableId)
    {
        $restaurantTableRepository = $this->getDoctrine()->getRepository(RestaurantTable::class);
        $restaurantTable = $restaurantTableRepository->find($restaurantTableId);

        $this->addFlash(
            'success',
            'Restaurant table with id: ' . $restaurantTable->getId() . ' has been removed successfully!'
        );

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($restaurantTable);
        $entityManager->flush();

        return $this->redirectToRoute('app_restaurant_view', [
            'restaurantId' => $restaurantTable->getRestaurant()->getId()
        ]);
    }

    /**
     * @Route("/toggle-status/{restaurantTableId}", name="toggle_status")
     * @param $restaurantTableId
     * @return RedirectResponse
     */
    public function toggleTableStatus($restaurantTableId)
    {
        $restaurantTableRepository = $this->getDoctrine()->getRepository(RestaurantTable::class);
        $restaurantTable = $restaurantTableRepository->find($restaurantTableId);

        if (RestaurantTable::RESTAURANT_TABLE_STATUS_ACTIVE === $restaurantTable->getStatus()) {
            $restaurantTable->setStatus(RestaurantTable::RESTAURANT_TABLE_STATUS_INACTIVE);
        } else {
            $restaurantTable->setStatus(RestaurantTable::RESTAURANT_TABLE_STATUS_ACTIVE);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        return $this->redirectToRoute('app_restaurant_view', [
            'restaurantId' => $restaurantTable->getRestaurant()->getId()
        ]);
    }
}