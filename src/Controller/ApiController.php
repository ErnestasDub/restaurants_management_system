<?php

namespace App\Controller;

use App\Entity\Restaurant;
use App\Entity\RestaurantTable;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/api", name="api_")
 * @package App\Controller
 */
class ApiController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/restaurant-tables/{restaurantId}")
     * @param $restaurantId
     * @return Response
     */
    public function getRestaurantTables($restaurantId)
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $restaurantRepository = $this->getDoctrine()->getRepository(Restaurant::class);
        $restaurant = $restaurantRepository->find($restaurantId);
        $restaurantTables = $restaurant->getRestaurantTables();

        if (Restaurant::RESTAURANT_STATUS_ACTIVE === $restaurant->getStatus()) {
            $jsonRestaurantTables = $serializer->serialize($restaurantTables, 'json', [
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);

            return new Response($jsonRestaurantTables, 200, ['Content-Type' => 'application/json']);
        } else {
            return new Response('The restaurant is inactive', 409, ['Content-Type' => 'application/json']);
        }
    }
}