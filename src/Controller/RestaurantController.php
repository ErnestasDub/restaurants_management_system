<?php

namespace App\Controller;

use App\Entity\Restaurant;
use App\Entity\RestaurantTable;
use App\Form\RestaurantFormType;
use App\Form\RestaurantSearchFormType;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/restaurant", name="app_restaurant_")
 * @package App\Controller
 */
class RestaurantController extends AbstractController
{
    /**
     * @Route("/create", name="create")
     * @param Request $request
     * @param FileUploader $fileUploader
     * @return RedirectResponse|Response
     */
    public function createRestaurant(Request $request, FileUploader $fileUploader)
    {
        $restaurant = new Restaurant();
        $form = $this->createForm(RestaurantFormType::class, $restaurant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $imageFile */
            $imageFile = $form->get('image')->getData();

            if ($imageFile) {
                $restaurantImageFilename = $fileUploader->upload($imageFile);
                $restaurant->setImageFilename($restaurantImageFilename);
                $restaurant->setStatus(Restaurant::RESTAURANT_STATUS_ACTIVE);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($restaurant);
            $entityManager->flush();

            $this->addFlash(
                'success',
                'Restaurant with id: ' . $restaurant->getId() . ' has been created successfully!'
            );

            return $this->redirectToRoute('app_restaurant_list');
        }

        return $this->render('restaurant/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/list", name="list")
     * @param Request $request
     * @return Response
     */
    public function listRestaurants(Request $request)
    {
        $restaurantRepository = $this->getDoctrine()->getRepository(Restaurant::class);
        $restaurants = $restaurantRepository->findAll();
        $searchForm = $this->createForm(RestaurantSearchFormType::class);
        $searchForm->handleRequest($request);

        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $searchTerm = $searchForm->get('term')->getData();

            $restaurants = $restaurantRepository->findMatchingRestaurants($searchTerm);
        }

        return $this->render('restaurant/list.html.twig', [
            'restaurants' => $restaurants,
            'searchForm' => $searchForm->createView()
        ]);
    }

    /**
     * @Route("/view/{restaurantId}", name="view")
     * @param $restaurantId
     * @return Response
     */
    public function viewRestaurant($restaurantId)
    {
        $restaurantRepository = $this->getDoctrine()->getRepository(Restaurant::class);
        /** @var Restaurant $restaurant */
        $restaurant = $restaurantRepository->find($restaurantId);
        $reachedMaxActiveTablesCount = $this->isReachedActiveTablesCount($restaurant);

        return $this->render('restaurant/view.html.twig', [
            'restaurant' => $restaurant,
            'reachedMaxActiveTablesCount' => $reachedMaxActiveTablesCount
        ]);
    }

    /**
     * @Route("/edit/{restaurantId}", name="edit")
     * @param Request $request
     * @param FileUploader $fileUploader
     * @param $restaurantId
     * @return RedirectResponse|Response
     */
    public function editRestaurant(Request $request, FileUploader $fileUploader, $restaurantId)
    {
        $restaurantRepository = $this->getDoctrine()->getRepository(Restaurant::class);
        $restaurant = $restaurantRepository->find($restaurantId);
        $oldRestaurantImage = new File($this->getParameter('restaurants_image_directory').'/'.$restaurant->getImageFilename());

        $form = $this->createForm(RestaurantFormType::class, $restaurant, ['required_image_input' => false]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $imageFile */
            $imageFile = $form->get('image')->getData();

            if ($imageFile && $oldRestaurantImage !== $imageFile) {
                $restaurantImageFilename = $fileUploader->upload($imageFile);
                $restaurant->setImageFilename($restaurantImageFilename);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($restaurant);
            $entityManager->flush();

            $this->addFlash(
                'success',
                'Restaurant with id: ' . $restaurantId . ' has been edited successfully!'
            );

            return $this->redirectToRoute('app_restaurant_view', [
                'restaurantId' => $restaurant->getId()
            ]);
        }

        return $this->render('restaurant/edit.html.twig', [
            'form' => $form->createView(),
            'restaurant' => $restaurant
        ]);
    }

    /**
     * @Route("/remove/{restaurantId}", name="remove")
     * @param $restaurantId
     * @return RedirectResponse
     */
    public function removeRestaurant($restaurantId)
    {
        $restaurantRepository = $this->getDoctrine()->getRepository(Restaurant::class);
        $restaurant = $restaurantRepository->find($restaurantId);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($restaurant);
        $entityManager->flush();

        $this->addFlash(
            'success',
            'Restaurant with id: ' . $restaurantId . ' has been removed successfully!'
        );

        return $this->redirectToRoute('app_restaurant_list');
    }

    /**
     * @Route("/toggle-status/{restaurantId}", name="toggle_status")
     * @param $restaurantId
     */
    public function toggleRestaurantStatus($restaurantId)
    {
        $restaurantRepository = $this->getDoctrine()->getRepository(Restaurant::class);
        $restaurant = $restaurantRepository->find($restaurantId);

        if (Restaurant::RESTAURANT_STATUS_ACTIVE === $restaurant->getStatus()) {
            $restaurant->setStatus(Restaurant::RESTAURANT_STATUS_INACTIVE);
        } else {
            $restaurant->setStatus(Restaurant::RESTAURANT_STATUS_ACTIVE);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        return $this->redirectToRoute('app_restaurant_view', [
            'restaurantId' => $restaurant->getId()
        ]);
    }

    /**
     * @param Restaurant $restaurant
     * @return bool
     */
    protected function isReachedActiveTablesCount(Restaurant $restaurant)
    {
        $activeTablesCounter = 0;

        foreach ($restaurant->getRestaurantTables() as $table) {
            if (RestaurantTable::RESTAURANT_TABLE_STATUS_ACTIVE === $table->getStatus()) {
                $activeTablesCounter++;
            }
        }

        return $restaurant->getMaxTablesCount() <= $activeTablesCounter;
    }
}