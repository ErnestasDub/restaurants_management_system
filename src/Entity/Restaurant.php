<?php

namespace App\Entity;

use App\Repository\RestaurantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RestaurantRepository::class)
 */
class Restaurant
{
    const RESTAURANT_STATUS_ACTIVE = 'active';
    const RESTAURANT_STATUS_INACTIVE = 'inactive';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string")
     */
    private $imageFilename;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $identificationNumber;

    /**
     * @ORM\Column(type="integer")
     */
    private $maxTablesCount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity=RestaurantTable::class, mappedBy="restaurant")
     */
    private $restaurantTables;

    public function __construct()
    {
        $this->restaurantTables = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getImageFilename(): ?string
    {
        return $this->imageFilename;
    }

    /**
     * @param string $imageFilename
     * @return $this
     */
    public function setImageFilename(string $imageFilename): self
    {
        $this->imageFilename = $imageFilename;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdentificationNumber(): ?string
    {
        return $this->identificationNumber;
    }

    /**
     * @param string $identificationNumber
     * @return $this
     */
    public function setIdentificationNumber(string $identificationNumber): self
    {
        $this->identificationNumber = $identificationNumber;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaxTablesCount(): ?int
    {
        return $this->maxTablesCount;
    }

    /**
     * @param int $maxTablesCount
     * @return $this
     */
    public function setMaxTablesCount(int $maxTablesCount): self
    {
        $this->maxTablesCount = $maxTablesCount;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|RestaurantTable[]
     */
    public function getRestaurantTables(): Collection
    {
        return $this->restaurantTables;
    }

    /**
     * @param RestaurantTable $restaurantTable
     * @return $this
     */
    public function addRestaurantTable(RestaurantTable $restaurantTable): self
    {
        if (!$this->restaurantTables->contains($restaurantTable)) {
            $this->restaurantTables[] = $restaurantTable;
            $restaurantTable->setRestaurant($this);
        }

        return $this;
    }

    /**
     * @param RestaurantTable $restaurantTable
     * @return $this
     */
    public function removeRestaurantTable(RestaurantTable $restaurantTable): self
    {
        if ($this->restaurantTables->removeElement($restaurantTable)) {
            if ($restaurantTable->getRestaurant() === $this) {
                $restaurantTable->setRestaurant(null);
            }
        }

        return $this;
    }
}
